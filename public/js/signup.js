$(function(){

    $('#email').focusout(function(){
        if ($(this).val() == '') {
            err = 'Необходимо указать email'
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else if (!(validateEmail($(this).val()))) {
            err = 'Неверный формат email';
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else {
            $(this).siblings('.error_box').hide();
        } 
    })

    $('#username').focusout(function(){
        if ($(this).val() == '') {
            err = 'Необходимо указать имя пользователя'
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else if (!(validateUsername($(this).val()))) {
            err = 'Имя пользователя содержит недопустимые символы';
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else {
            $(this).siblings('.error_box').hide();
        } 
    })

    $('#password').focusout(function(){
        if ($(this).val() == '') {
            err = 'Необходимо придумать пароль'
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else if (!(validatePass($(this).val()))) {
            err = 'Пароль содержит недопустимые символы';
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else {
            $(this).siblings('.error_box').hide();
        } 
    })

    $('#password_con').focusout(function(){
        if ($(this).val() == '') {
            err = 'Необходимо подтвердить введеный пароль'
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else if (($(this).val()) != ($("#password").val())) {
            err = 'Введеный пароль и подтверждение не совпадают';
            $(this).siblings('.error_box').children('.error_box_text').html(err);
            $(this).siblings('.error_box').show();
        }
        else {
            $(this).siblings('.error_box').hide();
        }
    })
})

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(String(email).toLowerCase());
}


function validateUsername(username) {
    var re = /^([a-zA-Z])[a-zA-Z_-]*[\w_-]*[\S]$|^([a-zA-Z])[0-9_-]*[\S]$|^[a-zA-Z]*[\S]$/;
    return re.test(String(username).toLowerCase());
}


function validatePass(pass) {
    var re = /^([a-zA-Z0-9@*#])/;
    return re.test(String(pass).toLowerCase());
}

