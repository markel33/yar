var express = require('express'),
  app = express(),
  router = express.Router()
var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser')
var morgan = require('morgan');
var flash = require('flash');

var LocalStrategy = require('passport-local').Strategy;
var mysql = require('mysql');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root'
});



global.__basedir = __dirname;

var hbs = require('hbs');
app.set('view engine', 'hbs');
app.use(morgan('dev'));

app.use(
  session({
    secret: "iy98hcbh489n38984y4h498", // don't put this into your code at production.  Try using saving it into environment variable or a config file.
    resave: true,
    saveUninitialized: false
  })
);
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
  extended: true
}));




app.use(express.static(__basedir + '/public'))




//unprotected routes
app.use(require('./login'))
//require(__basedir + '/controllers/login')(session);



//protected routes//////////
app.use((req, res, next) => {
  if (req.session.user) {
    next();
  } else {
    res.status(401).send('Authrization failed! Please login');
  }
});

app.use(require('./account'))

////////////////////

app.listen(3000, function() {
  hbs.registerPartials(__basedir + '/views/partials');
  console.log('Listening on port 3000...')
})