var express = require('express'),
  router = express.Router()
var path = require('path');
var mysql = require('mysql');
var bcrypt = require('bcrypt');
var uniqid = require('uniqid');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root'
});


connection.query('USE yar');
var site_data = {};
//////

//var testRoutes = require(path.join(__basedir + '/controllers/login'));
//router.use('/login', require('./login'))


router.get('/', function (req, res) {
  res.render(path.join(__basedir + '/views/index.hbs'), { ss: req });
})


router.get('/login', function (req, res) {
  res.render(path.join(__basedir + '/views/auth.hbs'));
})

router.get('/signup', function (req, res) {
  res.render(path.join(__basedir + '/views/signup.hbs'));
})

router.post('/login', (req, res) => {
  let { email, password } = req.body;
  console.log(req.body);
  connection.query("SELECT * FROM users WHERE `email` = '" + email + "'", function (err, rows) {
    if (!err && (rows.length != 0)) {
      let passwordCheck = bcrypt.compareSync(password, rows[0].password);
      //let passwordCheck = (password == rows[0].password)
      console.log(rows[0]);
      if (passwordCheck) { // we are using bcrypt to check the password hash from db against the supplied password by user
        req.session.user = {
          email: rows[0].email,
          id: rows[0].ID
        }; // saving some user's data into user's session
        req.session.user.expires = new Date(
          Date.now() + 3 * 24 * 3600 * 1000 // session expires in 3 days
        );
        //res.status(200).send('You are logged in, Welcome!');
        site_data.authorised = true;
        site_data.username = email;



       // res.render(path.join(__basedir + '/views/account.hbs'), { ss: req, hide_account: true });
       res.redirect('../account')

      } else {
        res.status(401).send('incorrect password');
      }
    } else {
      console.log(err);
      res.status(401).send('invalid login credentials')
    }
  })
})


router.post('/signup', (req, res) => {
  let { username, email, password } = req.body; // this is called destructuring. We're extracting these variables and their values from 'req.body'

  let userData = {
    username,
    password: bcrypt.hashSync(password, 5), // we are using bcrypt to hash our password before saving it to the database
    email
  };

  connection.query("SELECT * FROM users WHERE `username` = '" + userData.username + "'", function (err, rows) {
    if (err)
      console.log(err);
    if (rows.length) {
      return res.status(409).send('user already exist!');
    } else {

      var userid = uniqid();
      var insertQuery = "INSERT INTO users " +
        "( `id`,`username`, `password`, `email` ) " +
        "values ('" + userid + "','" + userData.username + "','" + userData.password + "','" + userData.email + "')";

      connection.query(insertQuery,
        (function (userid) {
          return function (err, rows) {
            //requires some error handling
            var insertQuery = "INSERT INTO accounts " +
              "( `user_id`,`balance`) " +
              "values ('" + userid + "','"+0+"')";
            connection.query(insertQuery, function (err, rows, fields) {
              if (err)
                console.log(err);
              else
                return res.status(201).json('signup successful');
            });

          };
        })(userid));

    }

  })
})


router.all('/logout', (req, res) => {
  delete req.session.user; // any of these works
  req.session.destroy(); // any of these works
 // res.status(200).send('logout successful');
 res.redirect('../')
})



module.exports = router